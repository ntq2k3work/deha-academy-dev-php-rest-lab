<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;
    protected $table = 'persons';
    public $timestamps = false;
    protected $fillable = ['FirstName', 'LastName','Address', 'City'];

    public function searchByName($keyword)
    {
        $person = $this->where('FirstName', 'LIKE', '%' . $keyword . '%')
            ->orWhere('LastName', 'LIKE', '%' . $keyword . '%') -> get();
        return $person;
    }
}
