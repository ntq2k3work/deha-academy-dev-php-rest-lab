<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function responseApi($data = null, $status = 200, $message = "Sucesss")
    {
        return response()->json([
            "status" => $status,
            "message" => $message,
            "data" => $data
        ], $status);
    }
}
