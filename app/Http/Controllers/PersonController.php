<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePersonRequest;
use App\Http\Requests\UpdatePersonRequest;
use App\Http\Resources\PersonCollection;
use App\Http\Resources\PersonResource;
use App\Models\Person;
use Illuminate\Http\Response;

class PersonController extends Controller
{
    public function index()
    {
        $person  = Person::paginate(5) ;
        $rs = PersonResource::collection($person) -> response() -> getData(true);
        return $this->responseApi($rs);
    }
    public function store(CreatePersonRequest $request)
    {
        $person = Person::create($request -> validated());
        $status = Response::HTTP_CREATED;
        $message = "Successfully created";
        return $this->responseApi($person, $status, $message);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $person = Person::findOrFail($id);
        return $this -> responseApi($person);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePersonRequest $request, string $id)
    {
        $person = Person::findOrFail($id);
        $person -> update($request -> validated());
        $message = "Successfully updated";
        $status = Response::HTTP_OK;
        return $this->responseApi($person, $status, $message);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $person = Person::findOrFail($id);
        $person -> delete();
        $status = Response::HTTP_NO_CONTENT;
        $message = "Successfully deleted";
        return $this->responseApi(null, Response::HTTP_NO_CONTENT, $message);
    }
}
